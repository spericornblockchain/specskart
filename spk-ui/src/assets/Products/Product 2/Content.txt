Product Type:
	Vincent Chase 

Product Id : 
	135876

Product Name:
	Tortoise Grey Mirror Full Rim Cat Eye Medium (Size-57) Vincent Chase Minimal VC S12642 - C2 Sunglasses

Product Price:
	$899

Product Details:
	An epitome of panache and simplicity, Tortoise Grey Mirror Full Rim Cat Eye Medium (Size-57) Vincent Chase Minimal VC S12642 - C2 Sunglasses is an association of classic design and stylish charm. Don these without the worry of suiting it up with your dresses every time.

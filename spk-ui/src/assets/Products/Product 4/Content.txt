Product Type:
	Vincent Chase Polarized

Product Id : 
	135606

Product Name:
	Golden Black Grey Full Rim Cat Eye Medium (Size-58) Vincent Chase Polarized DivineVC S12624 - C3 Polarized Sunglasses

Product Price:
	$1199

Product Details:
	Add a pinch of mischief and lot of sophistication to your look with Golden Black Grey Full Rim Cat Eye Medium (Size-58) Vincent Chase Polarized DivineVC S12624 - C3 Polarized Sunglasses. These glasses render you a comfortable wearing experience by wrapping firmly around head.
